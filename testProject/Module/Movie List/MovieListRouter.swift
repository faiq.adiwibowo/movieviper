//
//  MovieListRouter.swift
//  MyMovieListApp
//
//  Created by faiq adi on 23/02/23.
//

import Foundation

protocol ListRouterInterface {
    func navigateToDetailWith(movieID: String)
}

final class ListRouter: NSObject {

    weak var presenter: ListPresenterInterface?
    private weak var viewController: MovieListView?
    
    static func setupModule() -> MovieListView {
        let vc = MovieListView()
        let interactor = ListInteractor()
        let router = ListRouter()
        let presenter = ListPresenter(interactor: interactor, router: router, view: vc)

        vc.presenter = presenter
        router.presenter = presenter
        interactor.output = presenter
        router.viewController = vc
        return vc
    }
}

extension ListRouter: ListRouterInterface {
    func navigateToDetailWith(movieID: String) {
        let vc = MovieDetailRouter.setupModule()
        vc.movieID = movieID
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
