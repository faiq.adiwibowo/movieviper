//
//  File.swift
//  MyMovieListApp
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit

protocol ListViewControllerInterface: class {
    func loadUpcomingMovies(upcomingMovies: UpcomingMovies)
    func loadNowPlayingMovies(nowPlayingMovies: NowPlayingMovies)
    func loadSearchingMovies(searchMovies: SearchMovies)
}

final class MovieListView: UIViewController {
    
    var presenter: ListPresenterInterface?
    private let listView: MovieListViewBuilder = MovieListViewBuilder()
    private let notificationCenter = NotificationCenter.default
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        presenter?.viewDidAppear()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = listView
        hideKeyboardWhenTappedAround()
        goToDetailsPage()
        startSearch()
        prepareNavBar()
    }
    
    @objc func startSearch(){
        listView.searchMovies = {[weak self] movieKeyword in
            self?.presenter?.searchingMovies(query: movieKeyword)
        }

    }
    
    func goToDetailsPage(){
        listView.rowTapped = {[weak self] movieID in
            guard let self = self else { return }
            self.presenter?.rowTapped(movieID: movieID)
        }
    }
    
    private func prepareNavBar() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        backItem.tintColor = Constants.Colors.backItemColor
        self.navigationItem.backBarButtonItem = backItem
    }
}

extension MovieListView: ListViewControllerInterface {
    func loadSearchingMovies(searchMovies: SearchMovies) {
        self.listView.searchMovieArray = searchMovies.results
        DispatchQueue.main.async {
            self.listView.searchTableView.reloadData()
        }
    }
    
    func loadNowPlayingMovies(nowPlayingMovies: NowPlayingMovies) {
        listView.nowPlayingMovieArray = nowPlayingMovies.results
    }
    
    func loadUpcomingMovies(upcomingMovies: UpcomingMovies) {
        self.listView.movieArray = upcomingMovies.results
        DispatchQueue.main.async {
            self.listView.tableView.reloadData()
        }
    }
    
}

