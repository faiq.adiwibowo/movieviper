//
//  GenreListPresenter.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class GenreListPresenter: VTPGenreListProtocol {
    
    var interactor: PTIGenreListProtocol?
    var view: PTVGenreListProtocol?
    var router: PTRGenreListProtocol?
    
    func getHitAPi(){
        interactor?.getHitAPI()
    }
    func selectGenre(genreId: String, genre: String, nav: UINavigationController){
        interactor?.selectGenre(genreId: genreId, genre: genre, nav: nav)
    }
}
extension GenreListPresenter: ITPGenreListProtocol{
    func hitAPISuccess(genreData: [GenreData]){
        //
        view?.showSuccessResponses(genreData: genreData)
    }
    func getDetailGenreSuccess(genreMovieData: [GenreResult], genreId: String, genreName: String, nav: UINavigationController){
        router?.getDetailGenreSuccess(genreMovieData: genreMovieData,genreId: genreId, genreName: genreName, nav: nav)
    }
    func hitAPIFailed(message: String){
        view?.hitAPIFailed(message: message)
    }
}
