//
//  GenreListRouter.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class GenreListRouter: PTRGenreListProtocol {
    
    var navigationController: UINavigationController?
    static func createGenreListModule() -> GenreListVC {
        let view = GenreListVC()
        
        let presenter = GenreListPresenter()
        let interactor : PTIGenreListProtocol = GenreListInteractor()
        let router : PTRGenreListProtocol = GenreListRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        view.presenter = presenter
        
        return view
    }
    
    func getOtherPages() {
        //
    }
    func getDetailGenreSuccess(genreMovieData: [GenreResult], genreId: String, genreName: String, nav: UINavigationController){
        let vc = GenreDetailRouter.createGenreDetailModule()
        vc.genreDetailList = genreMovieData
        vc.genreId = genreId
        vc.headerTitle.text = genreName
        vc.modalPresentationStyle = .fullScreen
        nav.present(vc, animated: true)
    }
}
