//
//  GenreCell.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class GenreCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .blue
        
        addSubview(cellContainer)
        addSubview(genreLabel)
        
        NSLayoutConstraint.activate([
            cellContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            cellContainer.topAnchor.constraint(equalTo: topAnchor),
            cellContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            cellContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            genreLabel.leadingAnchor.constraint(equalTo: cellContainer.leadingAnchor, constant: 12),
            genreLabel.trailingAnchor.constraint(equalTo: cellContainer.trailingAnchor, constant: -12),
            genreLabel.centerYAnchor.constraint(equalTo: cellContainer.centerYAnchor)
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK:   COMPONENT
    let cellContainer: UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .clear
        return view
    }()
    let genreLabel: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        return label
    }()
    
}
