//
//  File.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit


class GenreListVC: GenreListVCBuilder{
    
    var presenter: VTPGenreListProtocol?
    var genreDataList = [GenreData]()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        showProgress()
        presenter?.getHitAPi()
        tableView.delegate = self
        tableView.dataSource = self
    }
}
//MARK: Table View
extension GenreListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genreDataList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        cell?.preservesSuperviewLayoutMargins = false
        cell?.separatorInset = UIEdgeInsets.zero
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? GenreCell else {return}
        cell.genreLabel.text = genreDataList[indexPath.row].name
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row index = \(indexPath.row)")
        presenter?.selectGenre(genreId: "\(genreDataList[indexPath.row].id ?? 0)", genre: genreDataList[indexPath.row].name, nav: self.navigationController!)
    }
    
}
//MARK: Result From Api
extension GenreListVC: PTVGenreListProtocol{
    func showSuccessResponses(genreData: [GenreData]){
        genreDataList = genreData
        tableView.reloadData()
        hideProgress()
    }
    
    func hitAPIFailed(message: String){
        hideProgress()
        self.showAlert(message: message)
        
    }
}
