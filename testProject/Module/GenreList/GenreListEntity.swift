//
//  GenreListEntity.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation

struct GenreModel: Codable {
    let genres: [GenreData]
}

// MARK: - Genre
struct GenreData: Codable {
    let id: Int
    let name: String
}
