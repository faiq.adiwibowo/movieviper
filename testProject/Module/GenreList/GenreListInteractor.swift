//
//  GenreListInteractor.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import Alamofire
import UIKit

class GenreListInteractor: PTIGenreListProtocol {
    var presenter: ITPGenreListProtocol?
    
    func getHitAPI(){
        let parameters: [URLQueryItem]  = [
            URLQueryItem(name: "api_key", value: "e7452b02e4e38ec328090154c68c43a7")
        ]
        if let url = APIConstants.getLink(baseURL: .URL_BASE, mainEndpoint: .genre, endpoints: [.movie, .list]){
            NetworkManager.shared.apiWithModel(url: url, type: GenreModel.self, method: .get, queryItem: parameters, completion: {
                decodedResponse, jsonDict in
                    print("success")
                    DispatchQueue.main.async {
//                        print(jsonDict)
                        self.presenter?.hitAPISuccess(genreData: decodedResponse.genres ?? [GenreData]())
                    }
                
            }, errorState: {
                err in
                    print("ERROR")
                    print(err)
                self.presenter?.hitAPIFailed(message: "Failed Load Genre List")
            })
            
        }
    }
    func selectGenre(genreId: String, genre: String, nav: UINavigationController){

        Worker.shared.genreDetail(genreId: genreId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                let genreData : [GenreResult] = details.results ?? [GenreResult]()
                self?.presenter?.getDetailGenreSuccess(genreMovieData: genreData, genreId: genreId, genreName: genre, nav: nav)
            case .failure(let error):
                print("ERROR")
                self?.presenter?.hitAPIFailed(message: "Failed Load Genre Detail")
            }
        }
        
    }
}
