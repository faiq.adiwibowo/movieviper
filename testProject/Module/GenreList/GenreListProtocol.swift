//
//  GenreListProtoco;.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

//MARK: View -> Presenter
protocol VTPGenreListProtocol: AnyObject {
    var view: PTVGenreListProtocol? {get set}
    var router: PTRGenreListProtocol? {get set}
    var interactor: PTIGenreListProtocol? {get set}
    
    func getHitAPi()
    func selectGenre(genreId: String, genre: String, nav: UINavigationController)
}
//MARK: Presenter -> Interactor
protocol PTIGenreListProtocol: AnyObject {
    var presenter:ITPGenreListProtocol? {get set}
    func getHitAPI()
    func selectGenre(genreId: String, genre: String, nav: UINavigationController)
}
//MARK: Interactor -> Presenter
protocol ITPGenreListProtocol: AnyObject {
    func hitAPISuccess(genreData: [GenreData])
    func getDetailGenreSuccess(genreMovieData: [GenreResult], genreId: String, genreName: String, nav: UINavigationController)
    func hitAPIFailed(message: String)
}
//MARK: Presenter -> Router
protocol PTRGenreListProtocol: AnyObject {
    func getOtherPages()
    func getDetailGenreSuccess(genreMovieData: [GenreResult], genreId: String, genreName: String, nav: UINavigationController)
}
//MARK: Presenter -> View
protocol PTVGenreListProtocol: AnyObject{
    func showSuccessResponses(genreData: [GenreData])
    func hitAPIFailed(message: String)
}
