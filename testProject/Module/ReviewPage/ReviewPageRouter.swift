//
//  ReviewPageRouter.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class ReviewPageRouter: PTRReviewPageProtocol {
    func getOtherPages() {
        //
    }
    
    let nav: UINavigationController? = nil
    
    static func createReviewPageModule() -> ReviewPageVC {
        let view = ReviewPageVC()
        
        let presenter = ReviewPagePresenter()
        let interactor : PTIReviewPageProtocol = ReviewPageInteractor()
        let router : PTRReviewPageProtocol = ReviewPageRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        view.presenter = presenter
        
        return view
    }
}
