//
//  ReviewCell.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class ReviewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        addSubview(cellContainer)
        addSubview(profileImage)
        addSubview(profileName)
        addSubview(reviewDate)
        addSubview(reviewContent)
        
        NSLayoutConstraint.activate([
            cellContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            cellContainer.topAnchor.constraint(equalTo: topAnchor),
            cellContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            cellContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            profileImage.leadingAnchor.constraint(equalTo: cellContainer.leadingAnchor, constant: 10),
            profileImage.topAnchor.constraint(equalTo: cellContainer.topAnchor, constant: 10),
            
            profileName.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10),
            profileName.topAnchor.constraint(equalTo: cellContainer.topAnchor, constant: 10),
            
            reviewDate.leadingAnchor.constraint(equalTo: profileName.trailingAnchor, constant: 10),
            reviewDate.topAnchor.constraint(equalTo: profileName.topAnchor),
            
            reviewContent.leadingAnchor.constraint(equalTo: profileName.leadingAnchor),
            reviewContent.topAnchor.constraint(equalTo: profileName.bottomAnchor, constant: 12),
            reviewContent.trailingAnchor.constraint(equalTo: cellContainer.trailingAnchor, constant: -15),
            reviewContent.bottomAnchor.constraint(equalTo: cellContainer.bottomAnchor, constant: -10)
            
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK:   COMPONENT
    let cellContainer: UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .white
        return view
    }()
    let profileImage: UIImageView = {
        let imageView = UIImageView()
        imageView.programatically()
        imageView.contentMode = .scaleAspectFit
        imageView.addBorder(color: .orange)
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        imageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        return imageView
    }()
    let profileName: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        return label
    }()
    let reviewDate: UILabel = {
        let label = UILabel()
        label.programatically()
        label.text = ""
        label.textColor = .systemGray
        label.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        return label
     }()
    let reviewContent: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = ""
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        return label
    }()
}
