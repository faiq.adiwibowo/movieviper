//
//  ReviewPageVCBuilder.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class ReviewPageVCBuilder: BaseViewController {
    
    var tableView : UITableView!
    var cellId = "cell"
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupLayout()
        view.backgroundColor = .darkGray
        view.bringSubviewToFront(backgroundLoading)
    }
    func setupLayout(){
        tableView = UITableView()
        tableView.programatically()
        
        tableView.register(ReviewCell.self, forCellReuseIdentifier: cellId)
        
        tableView.separatorStyle = .singleLineEtched
//        tableView.allowsSelection = false
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.layer.borderWidth = 1
        tableView.layer.cornerRadius = 5
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
