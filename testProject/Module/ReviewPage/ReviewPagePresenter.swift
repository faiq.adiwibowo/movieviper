//
//  ReviewPagePresenter.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation

class ReviewPagePresenter: VTPReviewPageProtocol {
    
    var interactor: PTIReviewPageProtocol?
    var view: PTVReviewPageProtocol?
    var router: PTRReviewPageProtocol?
    
    func fetchReviews(movieId: String){
        interactor?.fetchReviews(movieId: movieId)
    }
}
extension ReviewPagePresenter: ITPReviewPageProtocol{
    func hitAPISuccess(reviewData: [ReviewResult]){
        view?.showSuccessResponses(reviewData: reviewData)
    }
    
    func hitAPIFailed(message: String){
        view?.hitAPIFailed(message: message)
    }
}
