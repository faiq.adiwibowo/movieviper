//
//  ReviewPageVC.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit
import Kingfisher

class ReviewPageVC: ReviewPageVCBuilder{
    
    var presenter: VTPReviewPageProtocol?
    
    var reviewDataList = [ReviewResult]()
    var movieId = ""
    var indexSelected : Int?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        hideProgress()
        tableView.delegate = self
        tableView.dataSource = self
        presenter?.fetchReviews(movieId: movieId)
    }
}

//MARK: Table View
extension ReviewPageVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewDataList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        cell?.contentView.isUserInteractionEnabled = false
        cell?.preservesSuperviewLayoutMargins = false
        cell?.separatorInset = UIEdgeInsets.zero
        cell?.layoutMargins = UIEdgeInsets.zero
        return cell ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ReviewCell else {return}
        
        let resource = URL(string: Constants.BaseURL.imageBaseURL + (reviewDataList[indexPath.row].authorDetails.avatarPath ?? Constants.BaseURL.noImage))
        let placeholder = UIImage(named: "profileImage")
        cell.profileImage.kf.setImage(with: resource, placeholder: placeholder)
        cell.profileName.text = reviewDataList[indexPath.row].authorDetails.username
        cell.reviewDate.text = "123"
        cell.reviewContent.text = reviewDataList[indexPath.row].content
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == indexSelected {
            return UITableView.automaticDimension
        } else {
            return 100
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row index = \(indexPath.row)")
        indexSelected = indexPath.row
        tableView.reloadData()
    }
    
}
//MARK: Result From Api
extension ReviewPageVC: PTVReviewPageProtocol{
    func showSuccessResponses(reviewData: [ReviewResult]){
        reviewDataList = reviewData
        tableView.reloadData()
        if reviewDataList.isEmpty {
            let alert = CustomAlert(frame: self.view.frame, message: "No Review Available", buttonTitle: "OK")
            alert.delegate = self
            self.view.addSubview(alert)
        }
        hideProgress()
    }
    func hitAPIFailed(message: String){
        let alert = CustomAlert(frame: self.view.frame, message: message, buttonTitle: "OK")
        alert.delegate = self
        self.view.addSubview(alert)
        hideProgress()
    }
}
//MARK: Set PopUp Action
extension ReviewPageVC: CustomAlertProtocol{
    func popUpDisapear() {
        self.dismiss(animated: true)
    }
   
}
