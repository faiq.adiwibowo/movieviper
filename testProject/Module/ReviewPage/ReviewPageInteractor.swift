//
//  ReviewPageInteractor.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation

class ReviewPageInteractor: PTIReviewPageProtocol {
    var presenter: ITPReviewPageProtocol?
 
    func fetchReviews(movieId: String) {
        Worker.shared.reviewMovies(movieId: movieId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                let reviewResponse : [ReviewResult] = details.results 
                self?.presenter?.hitAPISuccess(reviewData: reviewResponse)
            case .failure(let error):
                print("error gais")
                self?.presenter?.hitAPIFailed(message: "Failed Get Review Data")
            }
        }
    }
}
