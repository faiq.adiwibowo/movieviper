//
//  ReviewPageProtocol.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

protocol VTPReviewPageProtocol: AnyObject {
    var view: PTVReviewPageProtocol? {get set}
    var router: PTRReviewPageProtocol? {get set}
    var interactor: PTIReviewPageProtocol? {get set}
    
    func fetchReviews(movieId: String)
}
protocol PTIReviewPageProtocol: AnyObject {
    var presenter:ITPReviewPageProtocol? {get set}
    func fetchReviews(movieId: String)
}
protocol ITPReviewPageProtocol: AnyObject {
    func hitAPISuccess(reviewData: [ReviewResult])
    func hitAPIFailed(message: String)
}
protocol PTRReviewPageProtocol: AnyObject {
    func getOtherPages()
}
protocol PTVReviewPageProtocol: AnyObject{
    func showSuccessResponses(reviewData: [ReviewResult])
    func hitAPIFailed(message: String)
}
