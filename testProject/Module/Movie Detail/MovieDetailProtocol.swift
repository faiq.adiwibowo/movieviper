//
//  MovieDetailProtocol.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation

protocol MovieDetailViewControllerInterface: class {
    func prepareMovieDetail(movieDetail: MovieDetails)
    func loadSimilarMovies(similarMovies: SimilarMovies)
    func prepareTrailerData(trailers: TrailerEntity)
    
    func failedFetchData(message: String)
}
protocol MovieDetailInteractorInterface: class {
    func fetchMovieDetails(movieId: String)
    func fetchSimilarMovies(movieId: String)
    func fetchTrailerMovies(movieId: String)
}
protocol MovieDetailInteractorOutputProtocol: class {
    func movieDetailFetchedSuccessfully(details: MovieDetails)
    func movieDetailFetchingFailed(withError error: Error)
    func similarMoviesFetchedSuccessfully(similars: SimilarMovies)
    func similarMoviesFetchingFailed(withError error: Error)
    func trailerFetchedSuccess(trailers: TrailerEntity)
    func trailerMoviesFetchingFailed(withError error: Error)
}
protocol MovieDetailPresenterInterface: class {
    func fetchDetails(movieId: String)
    func fetchSimilars(movieId: String)
    func fetchTrailer(movieId: String)
}
protocol MovieDetailRouterInterface: class {

}
