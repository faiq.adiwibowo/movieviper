//
//  MovieDetailPresenter.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation

final class MovieDetailPresenter {

    unowned var view: MovieDetailViewControllerInterface
    let router: MovieDetailRouterInterface?
    let interactor: MovieDetailInteractorInterface?
    
    init(interactor: MovieDetailInteractorInterface, router: MovieDetailRouterInterface, view: MovieDetailViewControllerInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension MovieDetailPresenter: MovieDetailPresenterInterface {
    func fetchDetails(movieId: String) {
        self.interactor?.fetchMovieDetails(movieId: movieId)
    }
    
    func fetchSimilars(movieId: String) {
        DispatchQueue.main.async {
            self.interactor?.fetchSimilarMovies(movieId: movieId)
        }
    }
    
    func fetchTrailer(movieId: String) {
        self.interactor?.fetchTrailerMovies(movieId: movieId)
    }
}

extension MovieDetailPresenter: MovieDetailInteractorOutputProtocol {
    
    func similarMoviesFetchedSuccessfully(similars: SimilarMovies) {
        view.loadSimilarMovies(similarMovies: similars)
    }
    
    func similarMoviesFetchingFailed(withError error: Error) {
        debugPrint(error)
        view.failedFetchData(message: "Failed Load Similiar Movie")
    }
    
    func movieDetailFetchedSuccessfully(details: MovieDetails) {
        view.prepareMovieDetail(movieDetail: details)
    }
    
    func movieDetailFetchingFailed(withError error: Error) {
        debugPrint(error)
        view.failedFetchData(message: "Failed Load Detail Movie")
    }
    
    func trailerFetchedSuccess(trailers: TrailerEntity){
        view.prepareTrailerData(trailers: trailers)
    }
    
    func trailerMoviesFetchingFailed(withError error: Error){
        debugPrint(error)
        view.failedFetchData(message: "Failed Load Trailer Source")
    }
}
