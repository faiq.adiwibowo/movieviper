//
//  MovieDetailViewControllerBuilder.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation
import UIKit
import WebKit

class MovieDetailView: UIView {
    let cellId = "cellId"
    var similarMoviesArray = [SimilarMoviesResult]()
    var imdbID = ""
    var rowTapped : ((String) -> Void)? = nil

    lazy var movieImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.layer.masksToBounds = true
        iv.image = UIImage(named: "header")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    lazy var headerLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.text = "The Lord Of The Rings The Lord Of The Rings"
        label.textColor = Constants.Colors.mainTextColor
        label.font = Constants.Fonts.headerFont
        return label
    }()
    
    lazy var descriptionText:UITextView = {
        let textView = UITextView()
        textView.isSelectable = false
        textView.textColor = Constants.Colors.descriptionTextColor
        textView.font = Constants.Fonts.defaultFont
        return textView
    }()

    lazy var dateLabel:UILabel = {
        let label = UILabel()
        label.textColor = Constants.Colors.descriptionTextColor
        label.text = "12.02.2020"
        label.font = Constants.Fonts.dateFont
        return label
    }()

    lazy var ratingLabel:UILabel = {
        let label = UILabel()
        label.textColor = Constants.Colors.descriptionTextColor
        label.font = Constants.Fonts.dateFont
        return label
    }()
    
    lazy var ratingImage : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: "rating")
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.isPagingEnabled = true
        return cv
    }()

    lazy var imdbButton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named:"imdb"), for: .normal)
        button.addTarget(self, action: #selector(goToIMDB), for: .touchUpInside)
        return button
    }()
    let backButton : UIButton = {
        let button = UIButton()
        button.programatically()
        button.backgroundColor = .systemGreen
        button.layer.cornerRadius = 5
//        button.addBorder(color: .lightGray)
        button.setTitle("Back", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        button.setTitleColor(.black, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowRadius = 2
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        return button
    }()
    let reviewButton : UIButton = {
        let button = UIButton()
        button.programatically()
        button.backgroundColor = .yellow
        button.layer.cornerRadius = 5
//        button.addBorder(color: .lightGray)
        button.setTitle("Reviews", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        button.setTitleColor(.black, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowRadius = 2
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        return button
    }()
    let trailerButton : UIButton = {
        let button = UIButton()
        button.programatically()
        button.backgroundColor = .systemRed
        button.layer.cornerRadius = 5
//        button.addBorder(color: .lightGray)
        button.setTitle("PLAY TRAILER", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        button.setTitleColor(.white, for: .normal)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowRadius = 2
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        return button
    }()
    let trailerView: UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .clear
        return view
    }()
    
    @objc func goToIMDB(){
        if let url = URL(string: "https://www.imdb.com/title/" + imdbID) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}

}

extension MovieDetailView: SetupView {
    func buildViewHierarchy() {

        self.addSubviews(movieImage, backButton, trailerView, trailerButton, headerLabel, descriptionText, dateLabel, imdbButton, ratingLabel, collectionView, ratingImage, reviewButton)
    }
    
    func setupConstraints() {
        let safeArea = self.safeAreaLayoutGuide

        movieImage.anchor(top: safeArea.topAnchor, leading: safeArea.leadingAnchor, bottom: nil, trailing: safeArea.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 240))
        
        trailerView.anchor(top: safeArea.topAnchor, leading: safeArea.leadingAnchor, bottom: nil, trailing: safeArea.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 240))
        
        backButton.anchor(top: movieImage.bottomAnchor, leading: safeArea.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 20, bottom: 0, right: 20), size: .init(width: 60, height: 30))
        
        trailerButton.anchor(top: movieImage.bottomAnchor, leading: nil, bottom: nil, trailing: safeArea.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 20), size: .init(width: 150, height: 30))
       
        reviewButton.anchor(top: nil, leading: backButton.trailingAnchor, bottom: nil, trailing: trailerButton.leadingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 5), size: .init(width: 150, height: 30))
        
        reviewButton.centerYAnchor.constraint(equalTo: trailerButton.centerYAnchor).isActive = true
        
        headerLabel.anchor(top: trailerButton.bottomAnchor, leading: safeArea.leadingAnchor, bottom: nil, trailing: safeArea.trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: 20))
        
        descriptionText.anchor(top: headerLabel.bottomAnchor, leading: safeArea.leadingAnchor, bottom: nil, trailing: safeArea.trailingAnchor, padding: .init(top: 20, left: 20, bottom: 0, right: 20))
        
        imdbButton.anchor(top: descriptionText.bottomAnchor, leading: nil, bottom: nil, trailing: safeArea.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 20),size: .init(width: 50, height: 25))
        
        dateLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: imdbButton.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        dateLabel.centerYAnchor.constraint(equalTo: imdbButton.centerYAnchor).isActive = true
        
        ratingLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: dateLabel.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 20))
        ratingLabel.centerYAnchor.constraint(equalTo: imdbButton.centerYAnchor).isActive = true
        
        ratingImage.anchor(top: nil, leading: nil, bottom: nil, trailing: ratingLabel.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 5), size: .init(width: 15, height: 15))
        ratingImage.centerYAnchor.constraint(equalTo: imdbButton.centerYAnchor).isActive = true
        
        collectionView.anchor(top: imdbButton.bottomAnchor, leading: safeArea.leadingAnchor, bottom: safeArea.bottomAnchor, trailing: safeArea.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 20, right: 0),size: .init(width: 0, height: 150))
    }
    
    func setupAdditionalConfiguration() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SimilarMoviesCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    
}

extension MovieDetailView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return similarMoviesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SimilarMoviesCollectionViewCell
        cell.configure(similarMovie: similarMoviesArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let rowTapped = self.rowTapped {
            rowTapped(String(similarMoviesArray[indexPath.row].id))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
