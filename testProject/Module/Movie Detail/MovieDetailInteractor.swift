//
//  MovieDetailInteractor.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation

final class MovieDetailInteractor {
    weak var output: MovieDetailInteractorOutputProtocol?
}

extension MovieDetailInteractor: MovieDetailInteractorInterface {
    func fetchSimilarMovies(movieId: String) {
        DispatchQueue.main.async {
            Worker.shared.similarMovies(movieId: movieId) { [weak self] result in
                guard self != nil else { return }
                switch result {
                case .success(let similars):
                    self?.output?.similarMoviesFetchedSuccessfully(similars: similars)
                case .failure(let error):
                    self?.output?.similarMoviesFetchingFailed(withError: error)
                    debugPrint(error)
                }
            }
        }
    }
    
    func fetchMovieDetails(movieId: String) {
        Worker.shared.movieDetail(movieId: movieId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                self?.output?.movieDetailFetchedSuccessfully(details: details)
            case .failure(let error):
                self?.output?.movieDetailFetchingFailed(withError: error)
            }
        }
    }
    
    func fetchTrailerMovies(movieId: String) {
        Worker.shared.trailerMovies(movieId: movieId) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                self?.output?.trailerFetchedSuccess(trailers: details)
            case .failure(let error):
                self?.output?.trailerMoviesFetchingFailed(withError: error)
            }
        }
    }
}
