//
//  MovieDetailViewController.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation
import UIKit
import Kingfisher
import WebKit

final class MovieDetailViewController: BaseViewController {
    
    var presenter: MovieDetailPresenterInterface?
    private let detailView: MovieDetailView = MovieDetailView()
    var movieID = String()
    var dataFetched : ((MovieDetails) -> Void)? = nil
    var webPlayer: WKWebView!
    var isTrailerAvailable = false

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        newRequest()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupLoadingView()
        showProgress()
        presenter?.fetchDetails(movieId: movieID)
        presenter?.fetchSimilars(movieId: movieID)
        presenter?.fetchTrailer(movieId: movieID)
    }
    
    override func viewDidLoad() {
        self.view = detailView
        newRequest()
        self.detailView.trailerView.isHidden = true
        detailView.reviewButton.addTarget(self, action: #selector(showReviewPage), for: .touchUpInside)
        detailView.trailerButton.addTarget(self, action: #selector(playTrailer), for: .touchUpInside)
        detailView.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    @objc func backAction(){
        UIView.animate(withDuration: 0.2, animations: {
                   self.detailView.backButton.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                   self.detailView.backButton.alpha = 0.7
               }) { (_) in
                   UIView.animate(withDuration: 0.2, animations: {
                       self.detailView.backButton.transform = CGAffineTransform.identity
                       self.detailView.backButton.alpha = 1.0
                   })
               }
        
        self.dismiss(animated: true)
    }
//MARK: Action to show Review Page
    @objc func showReviewPage(){
        UIView.animate(withDuration: 0.2, animations: {
                   self.detailView.reviewButton.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                   self.detailView.reviewButton.alpha = 0.7
               }) { (_) in
                   UIView.animate(withDuration: 0.2, animations: {
                       self.detailView.reviewButton.transform = CGAffineTransform.identity
                       self.detailView.reviewButton.alpha = 1.0
                   })
               }
        
        let vc = ReviewPageRouter.createReviewPageModule()
        vc.movieId = movieID
        present(vc, animated: true)
    }
//MARK: Action to Play Trailer
    @objc func playTrailer(){
        UIView.animate(withDuration: 0.2, animations: {
                   self.detailView.trailerButton.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                   self.detailView.trailerButton.alpha = 0.7
               }) { (_) in
                   UIView.animate(withDuration: 0.2, animations: {
                       self.detailView.trailerButton.transform = CGAffineTransform.identity
                       self.detailView.trailerButton.alpha = 1.0
                   })
               }
        if isTrailerAvailable {
            let labelAlert = UILabel()
            self.showToast(labelAlert, message: "No Trailer Available", constaint: [
                labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
            ])
        } else {
            self.detailView.trailerView.isHidden = false
        }
        
    }
    private func newRequest(){
        detailView.rowTapped = {[weak self] newID in
            guard let self = self else { return }
            self.presenter?.fetchDetails(movieId: newID)
            self.presenter?.fetchSimilars(movieId: newID)
            self.presenter?.fetchTrailer(movieId: newID)
        }
    }
}
//MARK: Set Data from Response
extension MovieDetailViewController: MovieDetailViewControllerInterface {
    func loadSimilarMovies(similarMovies: SimilarMovies) {
        detailView.similarMoviesArray = similarMovies.results
        self.detailView.trailerView.isHidden = true
        detailView.collectionView.reloadData()
    }
    
    func prepareMovieDetail(movieDetail: MovieDetails) {
        self.title = movieDetail.title
        detailView.headerLabel.text = movieDetail.title
        detailView.descriptionText.text = movieDetail.overview
        detailView.dateLabel.text = movieDetail.releaseDate
        detailView.ratingLabel.text = String(movieDetail.voteAverage)
        guard let resource = URL(string: Constants.BaseURL.imageBaseURL + (movieDetail.backdropPath ?? movieDetail.posterPath)) else {return}
        let placeholder = UIImage(named: "header")
        detailView.movieImage.kf.setImage(with: resource, placeholder: placeholder)
        detailView.imdbID = movieDetail.imdbID
        movieID = "\(movieDetail.id)"
    }

    func prepareTrailerData(trailers: TrailerEntity){
        if trailers.results.isEmpty {
            isTrailerAvailable = false
        } else {
            isTrailerAvailable = true
            
            let webConfiguration = WKWebViewConfiguration()
            webConfiguration.allowsInlineMediaPlayback = true
            
            DispatchQueue.main.async {
                self.webPlayer = WKWebView(frame: self.detailView.trailerView.bounds, configuration: webConfiguration)
                self.detailView.trailerView.addSubview(self.webPlayer)
                
                guard let videoURL = URL(string: "https://www.youtube.com/embed/\(trailers.results[0].key)") else { return }
                let request = URLRequest(url: videoURL)
                self.webPlayer.load(request)
            }
        }
        
        hideProgress()
    }
    
    func failedFetchData(message: String){
        self.showAlert(message: message)
        hideProgress()
    }
}
