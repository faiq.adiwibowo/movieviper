//
//  GenreDetailInteractor.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import Alamofire

class GenreDetailInteractor: PTIGenreDetailProtocol {
    var presenter: ITPGenreDetailProtocol?

    func loadMoreGenre(genreId: String, page: String){

        Worker.shared.genreDetail(genreId: genreId, page: page) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let details):
                let genreData : [GenreResult] = details.results ?? [GenreResult]()
                self?.presenter?.hitAPISuccess(genreData: genreData)
            case .failure(let error):
                print("ERROR")
                self?.presenter?.hitAPIFailed(message: "Failed Load More Data")
            }
        }
        
    }
}
