//
//  GenreDetailPresenter.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation

class GenreDetailPresenter: VTPGenreDetailProtocol {
    
    var interactor: PTIGenreDetailProtocol?
    var view: PTVGenreDetailProtocol?
    var router: PTRGenreDetailProtocol?
    
    func loadMoreGenre(genreId: String, page: String){
        interactor?.loadMoreGenre(genreId: genreId, page: page)
    }
}
extension GenreDetailPresenter: ITPGenreDetailProtocol{
    func hitAPISuccess(genreData: [GenreResult]){
        //
        view?.showSuccessResponses(genreData: genreData)
    }
    func hitAPIFailed(message: String){
        view?.hitAPIFailed(message: message)
    }
}
