//
//  GenreDetailRouter.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation

class GenreDetailRouter: PTRGenreDetailProtocol {
    
    
    static func createGenreDetailModule() -> GenreDetailVC {
        let view = GenreDetailVC()
        
        let presenter = GenreDetailPresenter()
        let interactor : PTIGenreDetailProtocol = GenreDetailInteractor()
        let router : PTRGenreDetailProtocol = GenreDetailRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        view.presenter = presenter
        
        return view
    }
    
    func getOtherPages() {
        let vc = MovieDetailRouter.setupModule()
    }
    func getDetailMovie(id: Int){
        let vc = MovieDetailRouter.setupModule()
//        vc.movieID = id
        
    }
}
