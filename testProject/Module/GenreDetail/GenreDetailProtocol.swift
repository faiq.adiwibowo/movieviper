//
//  GenreDetailProtocol.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation


//MARK: View -> Presenter
protocol VTPGenreDetailProtocol: AnyObject {
    var view: PTVGenreDetailProtocol? {get set}
    var router: PTRGenreDetailProtocol? {get set}
    var interactor: PTIGenreDetailProtocol? {get set}
    
    func loadMoreGenre(genreId: String, page: String)
}
//MARK: Presenter -> Interactor
protocol PTIGenreDetailProtocol: AnyObject {
    var presenter:ITPGenreDetailProtocol? {get set}
    
    func loadMoreGenre(genreId: String, page: String)
}
//MARK: Interactor -> Presenter
protocol ITPGenreDetailProtocol: AnyObject {
    func hitAPISuccess(genreData: [GenreResult])
    func hitAPIFailed(message: String)
}
//MARK: Presenter -> Router
protocol PTRGenreDetailProtocol: AnyObject {
    func getOtherPages()
}
//MARK: Presenter -> View
protocol PTVGenreDetailProtocol: AnyObject{
    func showSuccessResponses(genreData: [GenreResult])
    func hitAPIFailed(message: String)
}
