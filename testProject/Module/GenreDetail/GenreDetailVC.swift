//
//  GenreDetailVC.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit


class GenreDetailVC: GenreDetailVCBuilder{
    
    var presenter: VTPGenreDetailProtocol?
    var genreDetailList = [GenreResult]()
    var loadMoreControl: LoadMoreControl!
    var genreId = "0"
    var dataLoadLimit = 1
    
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        loadMoreControl = LoadMoreControl(scrollView: tableView, spacingFromLastCell: 10, indicatorHeight: 60)
        loadMoreControl.delegate = self
        
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    @objc func backAction(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK: Table View
extension GenreDetailVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genreDetailList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? ListMovieCell ?? ListMovieCell(style: .default, reuseIdentifier: cellId)
        cell.accessoryType = .disclosureIndicator
        cell.configureGenre(movieItem: genreDetailList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == self.tableView ? Constants.Sizes.tableViewCellHeight : Constants.Sizes.searchTableViewCellHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == self.tableView ? 250 : 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDetailMoview(movieId: "\(genreDetailList[indexPath.row].id ?? 0)")
    }
    
    @objc func showDetailMoview(movieId: String){
        let vc = MovieDetailRouter.setupModule()
        vc.movieID = movieId
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
}
//MARK: Load More
extension GenreDetailVC: UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        loadMoreControl.didScroll()
    }
    
}
extension GenreDetailVC: LoadMoreControlDelegate {
    func loadMoreControl(didStartAnimating loadMoreControl: LoadMoreControl) {
        print("didStartAnimating")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dataLoadLimit = self.dataLoadLimit + 1
            self.showProgress()
            self.presenter?.loadMoreGenre(genreId: self.genreId, page: String(self.dataLoadLimit))
        }
    }
    
    func loadMoreControl(didStopAnimating loadMoreControl: LoadMoreControl) {
        let labelAlert = UILabel()
        self.showToast(labelAlert, message: "Data Loaded", constaint: [
            labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
        ])
        self.hideProgress()
    }
}
//MARK: Result From Api
extension GenreDetailVC: PTVGenreDetailProtocol{
    func showSuccessResponses(genreData: [GenreResult]){
        if genreData.isEmpty{
            let labelAlert = UILabel()
            self.showToast(labelAlert, message: "No More Data Loaded", constaint: [
                labelAlert.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10)
            ])
        } else {
            for item in genreData {
                genreDetailList.append(item)
            }
        }
        
        tableView.reloadData()
        loadMoreControl.stop()
    }
    func hitAPIFailed(message: String){
        self.showAlert(message: message)
        tableView.reloadData()
        loadMoreControl.stop()
    }
}
