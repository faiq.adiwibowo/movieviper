//
//  GenreDetailVCBuilder.swift
//  testProject
//
//  Created by faiq adi on 03/03/23.
//

import Foundation
import UIKit

class GenreDetailVCBuilder: BaseViewController {
    
    var tableView : UITableView!
    var cellId = "cell"
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupLayout()
        view.bringSubviewToFront(backgroundLoading)
    }
    func setupLayout(){
        tableView = UITableView()
        tableView.programatically()
        
        tableView.register(ListMovieCell.self, forCellReuseIdentifier: cellId)
        
        tableView.separatorStyle = .singleLine
        
        view.addSubview(headerContainer)
        view.addSubview(backButton)
        view.addSubview(headerTitle)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            headerContainer.heightAnchor.constraint(equalToConstant: 80),
            headerContainer.widthAnchor.constraint(equalToConstant: view.frame.width),
            headerContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            headerContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            backButton.centerYAnchor.constraint(equalTo: headerContainer.centerYAnchor),
            backButton.leadingAnchor.constraint(equalTo: headerContainer.leadingAnchor, constant: 10),
            backButton.heightAnchor.constraint(equalToConstant: 40),
            backButton.widthAnchor.constraint(equalToConstant: 70),
            
            headerTitle.centerYAnchor.constraint(equalTo: headerContainer.centerYAnchor),
            headerTitle.centerXAnchor.constraint(equalTo: headerContainer.centerXAnchor),
            
            tableView.topAnchor.constraint(equalTo: headerContainer.bottomAnchor, constant: 10),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    var headerContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        view.programatically()
        return view
    }()
    let backButton : UIButton = {
        let button = UIButton()
        button.programatically()
        button.backgroundColor = .systemOrange
        button.layer.cornerRadius = 5
        button.addBorder(color: .lightGray)
        button.setTitle("Back", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        return button
    }()
    var headerTitle: UILabel = {
       let label = UILabel()
        label.programatically()
        label.text = "MOVIE GENRE"
        label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        return label
    }()
}
