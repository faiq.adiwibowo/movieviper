//
//  Network.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation
import Alamofire

enum Results<T> {
    case success(T)
    case failure(Error)
}

class NetworkManager {
    
    static let shared: NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()

    func request<T: Codable>(_ request: URLRequestConvertible, decodeToType type: T.Type, completionHandler: @escaping (Results<T>) -> ()) {
        AF.request(request).responseData { response in
            switch response.result {
            case .success(let data):
                do {
                    let decoded = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(.success(decoded))
                } catch {
                    completionHandler(.failure(error))
                }
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    func apiWithModel<T: Decodable>(url: URL, parameters: Parameters? = nil, type: T.Type, method: HTTPMethod, queryItem: [URLQueryItem] = [URLQueryItem](), completion: @escaping (T, NSDictionary) -> (), errorState: @escaping (Error?) -> () ) {
        //completion -> data dict sesuai model
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: nil,
            delegateQueue: nil)
        
        var request = URLRequest(url: url)
        
        if #available(iOS 16.0, *) {
            request.url?.append(queryItems: queryItem)
        } else {
            // Fallback on earlier versions
        }
        
        request.method = method
        do {
            if parameters != nil {
                request.httpBody   = try JSONSerialization.data(withJSONObject: parameters)
            }
           
        } catch let error {
            errorState(error)
        }

        let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                errorState(error)
            } else if let dataFromAPI = data{
                do{
                    let jsonData = try JSONSerialization.jsonObject(with: dataFromAPI)
                    let jsonDict = jsonData as! NSDictionary
                    
                    let decodedResponse = try JSONDecoder().decode(type.self, from: dataFromAPI)
                    
                    completion(decodedResponse, jsonDict)
                }catch let err{
                    print("fetch error, URL : " + url.absoluteString)
                    print(err)
                    print(err.localizedDescription)
                    errorState(err)
                }
            }
        })
        
        task.resume()
    }
    
}
