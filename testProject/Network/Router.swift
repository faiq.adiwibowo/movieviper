//
//  Router.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    
    case nowPlaying
    case upcoming
    case search(query: String)
    case details(movieId: String)
    case similars(movieId: String)
    case genreDetail(genreId: String, page: String)
    case review(movieId: String)
    case trailer(movieId: String)
    
    var method: HTTPMethod {
        switch self {
        case .nowPlaying:
            return .get
        case .upcoming:
            return .get
        case .search:
            return .get
        case .details:
            return .get
        case .similars:
            return .get
        case .genreDetail:
            return .get
        case .review:
            return .get
        case .trailer:
            return .get
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .nowPlaying:
            return nil
        case .upcoming:
            return nil
        case .search:
            return nil
        case .details:
            return nil
        case .similars:
            return nil
        case .genreDetail:
            return nil
        case .review:
            return nil
        case .trailer:
            return nil
        }
    }
    
    var url: URL {
        switch self {
        case .nowPlaying:
            let url = URL(string: Constants.BaseURL.nowPlayingURL)!
            return url
        case .upcoming:
            let url = URL(string: Constants.BaseURL.upcomingURL)!
            return url
        case .search(let query):
            let queryString = Constants.BaseURL.searchURL + query
            let url = URL(string: queryString)!
            return url
        case .details(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.detailString
            let url = URL(string: queryString)!
            return url
        case .similars(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + "/similar" + Constants.BaseURL.detailString
            let url = URL(string: queryString)!
            return url
        case .genreDetail(let genreId, let page):
            let queryString = Constants.BaseURL.baseGenreURL + genreId + "&page=" + page
            let url = URL(string: queryString)!
            print(url)
            return url
        case .review(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.reviewString
            let url = URL(string: queryString)!
            print(url)
            return url
        case .trailer(let movieId):
            let queryString = Constants.BaseURL.baseURL + movieId + Constants.BaseURL.trailerString
            let url = URL(string: queryString)!
            return url
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: parameters)
    }
}
