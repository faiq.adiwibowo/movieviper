
//
//  Requester.swift
//  MyMovieListApp
//
//  Created by faiq adi on 24/02/23.
//

import Foundation

class Worker {
    
    static let shared: Worker = {
        let instance = Worker()
        return instance
    }()

    func getUpcomingMovies(completionHandler: @escaping (Results<UpcomingMovies>) -> ()) {
        NetworkManager.shared.request(Router.upcoming, decodeToType: UpcomingMovies.self, completionHandler: completionHandler)
    }
    
    func getNowPlayingMovies(completionHandler: @escaping (Results<NowPlayingMovies>) -> ()) {
        NetworkManager.shared.request(Router.nowPlaying, decodeToType: NowPlayingMovies.self, completionHandler: completionHandler)
    }

    func searchMovies(query:String, completionHandler: @escaping (Results<SearchMovies>) -> ()) {
        NetworkManager.shared.request(Router.search(query: query), decodeToType: SearchMovies.self, completionHandler: completionHandler)
    }

    func movieDetail(movieId: String, completionHandler: @escaping (Results<MovieDetails>) -> ()) {
        NetworkManager.shared.request(Router.details(movieId: movieId), decodeToType: MovieDetails.self, completionHandler: completionHandler)
    }

    func similarMovies(movieId: String, completionHandler: @escaping (Results<SimilarMovies>) -> ()) {
        NetworkManager.shared.request(Router.similars(movieId: movieId), decodeToType: SimilarMovies.self, completionHandler: completionHandler)
    }
    
    func genreDetail(genreId: String, page: String = "1", completionHandler: @escaping (Results<GenreDetailModel>) -> ()) {
        NetworkManager.shared.request(Router.genreDetail(genreId: genreId, page: page), decodeToType: GenreDetailModel.self, completionHandler: completionHandler)
    }
    
    func reviewMovies(movieId: String, completionHandler: @escaping (Results<ReviewModel>) -> ()) {
        NetworkManager.shared.request(Router.review(movieId: movieId), decodeToType: ReviewModel.self, completionHandler: completionHandler)
    }
    
    func trailerMovies(movieId: String, completionHandler: @escaping (Results<TrailerEntity>) -> ()) {
        NetworkManager.shared.request(Router.trailer(movieId: movieId), decodeToType: TrailerEntity.self, completionHandler: completionHandler)
    }
}

