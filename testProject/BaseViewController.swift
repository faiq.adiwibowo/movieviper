//
//  BaseViewController.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {
    
    var viewWidth = 0.0
    var viewHeight = 0.0
    var loadingView: NVActivityIndicatorView!
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewWidth = view.frame.width
        viewHeight = view.frame.height
        
        view.backgroundColor = .white
        
//        setupLayoutView()
        setupLoadingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupLayoutView(){
        
    }
    func setupLoadingView() {
        
        loadingView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60), type: .lineSpinFadeLoader, color: UIColor.darkGray)
        backgroundLoading.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        blurLoading.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        loadingView.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
        loadingView.programatically()
        
        self.view.addSubview(backgroundLoading)
        self.view.addSubview(blurLoading)
        self.view.addSubview(loadingView)
        
        
        NSLayoutConstraint.activate([
            loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            backgroundLoading.topAnchor.constraint(equalTo: self.view.topAnchor),
            backgroundLoading.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            backgroundLoading.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            backgroundLoading.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            
            blurLoading.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            blurLoading.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            blurLoading.widthAnchor.constraint(equalToConstant: 100),
            blurLoading.heightAnchor.constraint(equalToConstant: 100),
        ])
        blurLoading.isHidden = true
        backgroundLoading.isHidden = true
    }
    
    func showProgress() {
        loadingView.startAnimating()
        loadingView.isHidden = false
        blurLoading.isHidden = false
        backgroundLoading.isHidden = false
    }
    
    func hideProgress() {
        loadingView.stopAnimating()
        loadingView.isHidden = true
        blurLoading.isHidden = true
        backgroundLoading.isHidden = true
    }
    
    let backgroundLoading : UIView = {
        let view = UIView()
        view.programatically()
        view.backgroundColor = .darkGray
        view.layer.opacity = 0.5
        view.setRadius(radius: 10)
        return view
    }()
    
    let blurLoading: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.programatically()
        blurEffectView.setRadius(radius: 10)
        return blurEffectView
    }()
}
