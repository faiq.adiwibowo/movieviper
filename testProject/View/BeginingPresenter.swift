//
//  BeginingPresenter.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation

class BeginingPresenter: VTPBeginingProtocol {
    
    var interactor: PTIBeginingProtocol?
    var view: PTVBeginingProtocol?
    var router: PTRBeginingProtocol?
    
    func getHitAPi(){
        interactor?.getHitAPI()
    }
}
extension BeginingPresenter: ITPBeginingProtocol{
    func hitAPISuccess(){
        //
    }
}
