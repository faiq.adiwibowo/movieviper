//
//  BeginingVCBuilder.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit

class BeginingVCBuilder: BaseViewController {
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setupLayout()
    }
    func setupLayout(){
        view.addSubview(backgroundContainer)
        
        NSLayoutConstraint.activate([
            backgroundContainer.heightAnchor.constraint(equalToConstant: view.frame.height/2),
            backgroundContainer.widthAnchor.constraint(equalToConstant: view.frame.width/2),
            backgroundContainer.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            backgroundContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    var backgroundContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        view.programatically()
        return view
    }()
}
