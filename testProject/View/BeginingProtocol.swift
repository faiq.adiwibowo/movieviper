//
//  BeginingProtocol.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit

protocol VTPBeginingProtocol: AnyObject {
    var view: PTVBeginingProtocol? {get set}
    var router: PTRBeginingProtocol? {get set}
    var interactor: PTIBeginingProtocol? {get set}
    
    func getHitAPi()
}
protocol PTIBeginingProtocol: AnyObject {
    var presenter:ITPBeginingProtocol? {get set}
    func getHitAPI()
    
}
protocol ITPBeginingProtocol: AnyObject {
    func hitAPISuccess()
}
protocol PTRBeginingProtocol: AnyObject {
    func getOtherPages()
}
protocol PTVBeginingProtocol: AnyObject{
    func showSuccessResponses()
}
