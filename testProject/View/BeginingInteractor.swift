//
//  File.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation

class BeginingInteractor: PTIBeginingProtocol {
    var presenter: ITPBeginingProtocol?
    
    func getHitAPI(){
        presenter?.hitAPISuccess()
    }
}
