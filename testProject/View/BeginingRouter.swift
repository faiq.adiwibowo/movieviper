//
//  BeginingRouter.swift
//  testProject
//
//  Created by faiq adi on 23/02/23.
//

import Foundation
import UIKit

class BeginingRouter: PTRBeginingProtocol {
    func getOtherPages() {
        //
    }
    
    let nav: UINavigationController? = nil
    
    static func createBeginingModule() -> BeginingVC {
        let view = BeginingVC()
        
        let presenter = BeginingPresenter()
        let interactor : PTIBeginingProtocol = BeginingInteractor()
        let router : PTRBeginingProtocol = BeginingRouter()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.presenter = presenter
        view.presenter = presenter
        
        return view
    }
}
