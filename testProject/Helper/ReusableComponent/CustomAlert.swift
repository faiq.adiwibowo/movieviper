//
//  CustomAlert.swift
//  testProject
//
//  Created by faiq adi on 04/03/23.
//

import Foundation
import UIKit

protocol CustomAlertProtocol{
    func popUpDisapear()
}
class CustomAlert: UIView{

    var actionButton: (() -> Void)?
    var isRefresh = false
    var widthConstant: CGFloat?
    var delegate: CustomAlertProtocol?
    
    var buttonOKWidthConstraint: NSLayoutConstraint!
    var contentWidthConstraint: NSLayoutConstraint!
    var stackContentWidthConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView(image: nil)
    }

    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, message: String, buttonTitle: String, completion: @escaping () -> Void) {
        super.init(frame: frame)
        
        labelMessage.text = message
        buttonOK.setTitle(buttonTitle, for: .normal)
        actionButton = completion
        
        setupView(image: nil)
    }
    
    init(message: String, buttonTitle: String){
        super.init(frame: CGRect.zero)
        self.programatically()
        
        labelMessage.text = message
        buttonOK.setTitle(buttonTitle, for: .normal)
        
        setupView(image: nil)
    }
    
    init(frame: CGRect, message: String, buttonTitle: String){
        super.init(frame: frame)
        
        labelMessage.text = message
        buttonOK.setTitle(buttonTitle, for: .normal)
        
        setupView(image: nil)
    }
    
    @objc func removeAction(sender: UIButton){
        self.removeFromSuperview()
        delegate?.popUpDisapear()
    }
    
    @objc func buttonAction(){
        actionButton?()
        self.removeFromSuperview()
        if isRefresh{
            self.removeFromSuperview()
        }
        delegate?.popUpDisapear()
    }

    func setupTarget(){
        if actionButton != nil{
            buttonOK.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        } else {
            buttonOK.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        }
    }
    
    func setupView(image: UIImage?){
        standardInstallation(image)
        
        setupContraint()
        setupTarget()
    }
    
    
    func standardInstallation(_ image: UIImage?){
        self.addSubview(blurView)
        self.addSubview(contentView)
        
        stackViewContent.addArrangedSubview(imageAlert)
        stackViewContent.addArrangedSubview(labelMessage)
        stackViewContent.addArrangedSubview(buttonOK)
        
        self.addSubview(stackViewContent)
        
        
        
        if image != nil{
            imageAlert.image = image
            imageAlert.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
            imageAlertHeightConstraint = imageAlert.heightAnchor.constraint(lessThanOrEqualToConstant: 400)
            imageAlertHeightConstraint.isActive = true
        } else {
            imageAlert.heightAnchor.constraint(equalToConstant: 0).isActive = true
            imageAlertHeightConstraint = imageAlert.heightAnchor.constraint(lessThanOrEqualToConstant: 0)
            imageAlertHeightConstraint.isActive = true
        }
    }
    
    
    var imageAlertHeightConstraint: NSLayoutConstraint!
    
    func setupContraint(){
        buttonOKWidthConstraint = buttonOK.widthAnchor.constraint(equalToConstant: (widthConstant ?? 300)-40)
        contentWidthConstraint = contentView.widthAnchor.constraint(equalToConstant: widthConstant ?? 300)
        stackContentWidthConstraint = stackViewContent.widthAnchor.constraint(equalToConstant: (widthConstant ?? 300)-50)
        
        NSLayoutConstraint.activate([
            blurView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            blurView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            blurView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            blurView.topAnchor.constraint(equalTo: self.topAnchor),
            contentView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            contentWidthConstraint,
            stackViewContent.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            stackViewContent.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            stackContentWidthConstraint,
            buttonOKWidthConstraint,
            buttonOK.heightAnchor.constraint(equalToConstant: 50),
            contentView.topAnchor.constraint(equalTo: stackViewContent.topAnchor, constant: -25),
            contentView.bottomAnchor.constraint(equalTo: stackViewContent.bottomAnchor, constant: 25)
        ])
    }
    
    

    let stackViewContent: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.programatically()
        stackView.spacing = 20
        return stackView
    }()
    
    let blurView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect.zero
        blurEffectView.programatically()
        return blurEffectView
    }()
    
    let contentView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        view.programatically()
        view.backgroundColor = .white
        view.setRadius(radius: 10)
        return view
    }()
    
    let titleLabelMessage: UILabel = {
        let label = UILabel()
        label.programatically()
//        label.textColor = .darkBlue
//        label.font = UIFont.openSansSemiBold(size: 18)
        label.numberOfLines = 5
        label.textAlignment = .center
        label.text = "GREAT!"
        return label
    }()
    
    let labelMessage: UILabel = {
        let label = UILabel()
        label.programatically()
//        label.textColor = .darkBlue
//        label.font = UIFont.openSansSemiBold(size: 17)
        label.numberOfLines = 5
        label.textAlignment = .center
        label.text = "Alert Message"
        return label
    }()
    
    let buttonOK: UIButton = {
        let button = UIButton()
        button.programatically()
        button.setTitle("OK", for: .normal)
        button.backgroundColor = .red
        button.frame.size = CGSize(width: 100, height: 50)
//        button.titleLabel?.font = UIFont.openSansSemiBold(size: 17)
        button.setRadius(radius: 50/2)
        return button
    }()
    
    let buttonNO: UIButton = {
        let button = UIButton()
        button.programatically()
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(UIColor.systemRed, for: .normal)
        button.backgroundColor = .white
        button.frame.size = CGSize(width: 100, height: 50)
//        button.titleLabel?.font = UIFont.openSansSemiBold(size: 17)
        button.setRadius(radius: 25)
        button.layer.borderColor = UIColor.systemRed.cgColor
        button.layer.borderWidth = 0.5
        return button
    }()
    
    let imageAlert: UIImageView = {
        let imageView = UIImageView()
        imageView.programatically()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
}
